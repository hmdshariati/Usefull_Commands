help to edit: https://gist.github.com/PurpleBooth/109311bb0361f32d87a2
# Usefull_Commands
I used to search in net for commands syntax. it's better to collect them for others.

# run a command in ubuntu
* ctrl+alt+T to open a terminal
* alt+F2 to execute a command without terminal

* update url of wordpress
* best way to replace all in editor

```
	sed -i -e 's/http\:\/\/url\.com/http\:\/\/localhost\:port/g' hello.txt
```
* or update database

```
	update wp_options set option_value = 'url:port' where option_name = 'siteurl';
	update wp_options set option_value = 'url:port' where option_name = 'home';
	UPDATE wp_links SET link_image = REPLACE(link_image,'old_url','');
	UPDATE wp_links SET link_url = REPLACE(link_url,'old_url','');
	UPDATE wp_posts SET post_content = REPLACE(post_content,'old_url','');
	UPDATE wp_postmeta SET meta_value = REPLACE(meta_value,'old_url','');
```

# split a movie
	ffmpeg -ss 00:00:00 -t 00:46:00 -i inputfile outputfile
* in error with :

```
	ffmpeg -ss 00:00:00 -t 00:46:00 -i inputfile -max_muxing_queue_size 400 outputfile
```

# find last change files
	find /<directory> -newermt "-24 hours" -ls

# about mail
pop3 can delete on serve but IMAP not.

# ssh login without anyone know
* at the end of ssh session :

	find / -mtime -1 |grep -v 'proc\|module\|cache\|lib\|kernel\|/sys/\|devices'
* rm files that think log you

	rm /var/log/wtmp /var/log/btmp /var/log/lastlog
	touch /var/log/wtmp /var/log/btmp /var/log/lastlog
	history -c

# change grub2 mode
	sudo vi /etc/default/grub
GRUB_CMDLINE_LINUX="text"

	update-grub2

# obtain JAVA_HOME etc.
	vi /etc/environment
	export

# send stdout & stderr to /dev/null
	> /dev/null 2>&1

# display colored in bash
	??

# docker
### install docker on ubuntu 16.04
	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
	sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
	sudo apt-get update
	sudo apt-get install -y docker-compose

### install docker-compose
	sudo curl -L https://github.com/docker/compose/releases/download/1.17.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
	sudo chmod +x /usr/local/bin/docker-compose
	docker-compose --version

### docker pull , export and import from host to host
	docker pull ubuntu
	docker run -t -i ubuntu /bin/bash

### default place of containers
```
/var/lib/docker
```

### after desired changes, in another bash
	docker ps
	docker export ID | gzip > ubuntu.tar.gz
### in another host
	gzcat ubuntu.tar.gz | docker import - ubuntu-alice
	docker save ubuntu | gzip > ubuntu-golden.tar.gz
	gzcat ubuntu-golden.tar.gz | docker load

### docker pull and transfer to another repo
	docker pull ubuntu
	docker tag ubuntu another_repo
	docker push another_repo


### import data to exist database in mysql/mariadb docker container
	docker exec -i container_name mysql -uUser -pPassword --database=DBName < sqlfile

### disable startup docker containers
* in /va/var/lib/docker/containers

	grep -irn '"RestartPolicy":{"Name":"always"' | cut -d \: -f 1
* the result list is files with restartpolicy; change them to "RestartPolicy":{"Name":"no"

	grep -irn '"RestartPolicy":{"Name":"always"' | cut -d \: -f 1 | xargs -n 1 sed -i s/'"RestartPolicy":{"Name":"always"'/'"RestartPolicy":{"Name":"no"'/g

* bash of docker container

	docker exec -it container_name bash

### docker network
* join to a predefined network

```
docker network ls

```

### start docker container with systemd in centos 7
* add container.service to /etc/systemd/system/

	[Unit]
	Description=Redis container
	After=docker.service

	[Service]
	Restart=always
	ExecStart=/usr/bin/docker start -a redis_server
	ExecStop=/usr/bin/docker stop -t 2 redis_server

	[Install]
	WantedBy=local.target

### change docker containers
* after changes:

	docker commit container_name desired_name

### multi (two and more) commands in docker container with exec option
	docker exec -ti [name_of_ocsrv_container] bash -c 'command1;command2;....'

### delete container
	docker rm ocserv

### stop and start docker daemon
	service docker stop
	service docker start

### check started containers
	docker ps

## open connect (openconnect)
### create new user for openconnect (ocsrv)
	docker exec -ti [name_of_ocsrv_container] ocpasswd -c /etc/ocserv/ocpasswd -g "Route,All" username

### connect to server from client with openconnect
	sudo openconnect [IP or URL] --user=username -p=password --authgroup=route    --servercert sha256:[has given on first use from bash]

### openconnect ocserv with docker

* create and run new ocserv
* https://github.com/TommyLau/docker-ocserv

	sudo docker run --name [name_of_ocsrv_container] --privileged -p 443:443 -p 443:443/udp -d tommylau/ocserv

### bash of ocserv
	docker exec -it [ocserv_container_name] bin/bash
	ps aux | grep ^test | cut -d \  -f 6

## gitlab runner
* Stop and remove the existing container:

	docker stop gitlab-runner && docker rm gitlab-runner

* Start the container as you did originally:

	docker run -d --name [container_name] --restart always -v /var/run/docker.sock:/var/run/docker.sock 	-v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner:latest

### with ssl
	docker run -d --name [container_name] --restart always 	-v /var/run/docker.sock:/var/run/docker.sock 	-v /srv/gitlab-runner/config:/etc/gitlab-runner:Z 	gitlab/gitlab-runner:latest

# link css
 	<link rel="stylesheet" type="text/css" href="theme.css">
# mongodb

# switched to mongodb shell
 mongo

# list all databases
 db.adminCommand('listDatabases')

# use a database
 use [dbname]

# show all collections
 show collections

# select all from a collection
 db.[collname].find()

# update a
 db.[collname].update(
    { "[field]" : "[citria(first)]" },
    {
      $set: { "[setfield]": "[updatedVal]" },
      $currentDate: { "lastModified": true } //current date
    }
)

db.users.update( {"username":"[username]"},
{$set :{"emails" : [
			{ "address" : "[email@domain.com]",
			 "verified" : true
			 }
		    ]
	}
})

# avoid duplicte value in bash
export HISTCONTROL=ignoreboth:erasedups # in .bash_rc

# unlimited bash history
export HISTFILESIZE=
export HISTSIZE=
# don’t store specific lines in bash history
HISTCONTROL=ignoreboth

# record timestamp for each line
HISTTIMEFORMAT='%F %T '

# save history immediately
PROMPT_COMMAND='history -a'

# append instead of writing
shopt -s histappend

# use one command per line
shopt -s cmdhist

# kill all instances of an app with bash
ps aux | grep [app_name] | cut -d \  -f 4,5,6 | xargs -n 1 kill -9

# usefull alias for ~/.bashrc or /etc/bash.bashrc
alias rm='rm -i'

# Prompt for history -c
* not sure to work
history(){
	if [ "$1" = "-c" ]
	then
		echo "are you sure to delete all history?(y/n):"
		if [ "$2" = "y" ]
		then
			history -c
		fi
	else
		history "$1"
	fi
}

# crontab for history
??

# diffrences between initd,SysV,systemD and upstart
initd: /etc/init.d/[services] start/stop/status/restart/reload
SysV: update-rc.d [service] remove/start/stop/status/restart/reload # or remove it from /etc/rc.*
SystemD: systemctl disable/enable/start/stop/status/restart/reload [services]
upstart: ??


# ignore labtop lid suspend on close
su -c 'vi /etc/systemd/logind.conf'

* and set HandleLidSwitch to ignore:
HandleLidSwitch=ignore
* and then restart systemd
su -c 'systemctl restart systemd-logind.service'

# login without password to ssh
ssh-keygen
ssh-copy-id user@host

# DirectAdmin multi php version

# SSL/TLS
* https
	- ??
* ssh
	- ??

# change ports with firewall (iptables-ufw-firewalld)
refrence: https://www.digitalocean.com/community/tutorials/how-to-set-up-a-basic-iptables-firewall-on-centos-6
* blocking null packets.

 			iptables -A INPUT -p tcp --tcp-flags ALL NONE -j DROP

The attack patterns use these to try and see how we configured the VPS and find out weaknesses
* reject syn-flood attack

 			iptables -A INPUT -p tcp ! --syn -m state --state NEW -j DROP

attackers open a new connection, but do not state what they want (ie. SYN, ACK, whatever)
* XMAS packets

 			iptables -A INPUT -p tcp --tcp-flags ALL ALL -j DROP

* Open all Local

 			iptables -A INPUT -i lo -j ACCEPT

* Allow webserver traffic

 			iptables -A INPUT -p tcp -m tcp --dport 80 -j ACCEPT
			iptables -A INPUT -p tcp -m tcp --dport 443 -j ACCEPT

* Allow email ports (SMTP,POP3,IMAP)

 			iptables -A INPUT -p tcp -m tcp --dport 25 -j ACCEPT
			iptables -A INPUT -p tcp -m tcp --dport 465 -j ACCEPT
			iptables -A INPUT -p tcp -m tcp --dport 110 -j ACCEPT
			iptables -A INPUT -p tcp -m tcp --dport 995 -j ACCEPT
			iptables -A INPUT -p tcp -m tcp --dport 143 -j ACCEPT
			iptables -A INPUT -p tcp -m tcp --dport 993 -j ACCEPT

* Allow SSH on desired port

 			iptables -A INPUT -p tcp -m tcp --dport [port] -j ACCEPT

* Allow SSH on desired port for my IP

 			iptables -A INPUT -p tcp -s YOUR_IP_ADDRESS -m tcp --dport [port] -j ACCEPT

* Allow us to use outgoing connections (ie. ping from VPS or run software updates);

 			iptables -I INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

* block everything else

			iptables -P OUTPUT ACCEPT
			iptables -P INPUT DROP		

# disable root login
ref: https://github.com/jrleszcz/linux-server-setup
* edit sshd file
		/etc/ssh/sshd_config		
		-change:

 			# Authentication:
			PermitRootLogin no

reload ssh

# apache
* list virtual hosts

	apachectl -M

* check configuration

	apachectl -t

# Git
* move current directory to remote git repository
* make a git repository in current directory

```
	git init .
```

* add server side url to current git repository as remote

```
	git remote add origin url
```

* add current files

```
	git add .
```

* commit

```
	git commit -m "first "
```

* push to server

```
	git push -u origin master
```

* git branch

```
git branch [branch_name]
git checkout [branch_name]
git checkout -f [branch_name]
```

* show configuration of git in current repository

```
	git config --get remote.origin.url
	git remote show origin
```

* update the list of git branches in local

```
git remote update origin --prune
```

* create new branch and switch on it

```
	git branch [new_branch_names]
	git checkout [new_branch_names]
```

or you can use this shorthand

```
	git checkout -b [new_branch_names]
```

* merge two branches (hotfix into master)

(https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging)

```
	git checkout master
	git merge iss53
```

in conflict use mergetool

```
git mergetool
```

* list all git brances

(http://gitready.com/intermediate/2009/02/13/list-remote-branches.html)

```
git branch
git branch -a
git branch -r #
```

* go back commit

```
git reset --hard 0d1d7fc32
```

for one branch back

```
git reset HEAD~ --hard
```


### git config for client

* your name and email address

```
git config --global user.name "Your Name"
git config --global user.email you@example.com
```

or

```
git config --global -e

```


* ??

(https://stackoverflow.com/a/47000543)

```

```

# laravel
### migration
* create new migration

for update:
```
	php artisan make:migration [some_name_as_comment] --table=[table_name]
```
https://laravel.com/docs/5.6/migrations

* generate migration in laravel
https://github.com/Xethron/migrations-generator

* create storage link

```
mv public/storage public/storage0
php artisan storage:link
/bin/cp -rf   public/storage0/*
/bin/cp -rf   public/storage_old/*
/bin/cp -rf   storage/users/
/bin/cp -rf   storage/companies/
rm -rf   public/storage0/*
rm -rf   public/storage_old/*
rm -rf   storage/users/
rm -rf   storage/companies/
chown ...
```

# laravel best practice
https://github.com/alexeymezenin/laravel-best-practices

# gitlab

show the version and other info
```
gitlab-rake gitlab:env:info
```

# vagrant

* create new Vagrantfile in current directory

```
vagrant init
```

* Vagrantfile simple example

```
Vagrant.configure("2") do |config|
  config.vm.box = "archlinux/archlinux"
end
```

* download and run Vagrant according to Vagrantfile in current directory

```
vagrant up
```

# ssh

* generate ssh key

```
ssh-keygen -t rsa -C "your.email@example.com" -b 4096
```

# gitlab

* push to gitlab if ssh port is blocked

create ~/.ssh/config

```
Host gitlab.com
  Hostname altssh.gitlab.com
  User git
  Port 443
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/id_rsa
```

* add public key in gitlab

```
??
```

test it:

```
ssh -T -p 443 git@altssh.gitlab.com
```

# rsync

```
rsync -anv --exclude=*~ dir1 dir2
```

# bash

* copy without prompt
```
/bin/cp -rf ...
```

# Shortcuts

## eclipse
* open type
```
Ctrl+Shift+t
```

* generate source (in oxygen version)
```
Alt+Shift+s
```

# processmaker notes

## backup
* first recognize size of processmaker directory
```
cd /opt/processmaker ; find . -type d -exec du -h {} + | grep '^[0-9].*G.*\.\/workflow' | sort | uniq
```

* then backup without huge uploaded and logged files
```
cd /opt/processmaker ; tar --exclude='./shared/sites/workflow/log' --exclude='./shared/sites/workflow/files' --exclude='./workflow/public_html/uploads' --exclude='*.git' -zcvf ../new.tar.gz ./
```


# manjaro (Arch Base Linux)

## some basic usefull command

### in manjaro: Install gnu-netcat for use nc

```
pacman -S gnu-netcat
```

# Network

##Linux

###find if a port of server is open or close(refuse)

```
nc -vz targetServer portNum
```

# hardware
* find the specification of RAM in linux

```
sudo dmidecode --type memory
```

#javascript usefull scripts

## Generate random number
```
var el = Math.round(new Date().getTime()/100000000+Math.random() * 100 );//5 number
```

# crwal
```

```


mysql -u <username> -p -h <host_name or ip> Then run

1 SET FOREIGN_KEY_CHECKS=0;

2 SOURCE /pathToFile/backup.sql;

3 SET FOREIGN_KEY_CHECKS=1;
# Networking
## linux
### debian based
```
vi /etc/network/interfaces
iface eth0 inet static
      address 10.1.1.125
      netmask 255.0.0.0
      gateway 10.1.1.1
```
https://wiki.debian.org/NetworkConfiguration

## monitoring
### windows
* monitor all ports in windows and their program and pid
```
netstat -aon
```
https://www.howtogeek.com/howto/28609/how-can-i-tell-what-is-listening-on-a-tcpip-port-in-windows/

# bash
## process
*find what is different  between result of df and du 
```
sudo lsof +L1
```
some files in sftp, tmp etc. use / (root) storage (https://serverfault.com/a/315945)

*find who are logeed in
```
w
```
and kill them with their pts
```
ps aux | grep pts
```

## usefull_commands_in_bash
* change file modification date. input 2 will change as input 1
```
touch -d @$(stat -c "%Y" $1) $2
```

# processmaker


# kubernetes

* check the cluster configs

```
kubectl cluster-info
```

or for debug

```
kubectl cluster-info debug
```

## minikube

run kubernetes cluster locally

* if this error occured: "Error starting host: Error getting state for host: machine does not exist." 

```
minikube delete
minikube start
```
